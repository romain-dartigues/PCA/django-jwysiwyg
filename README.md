# README

This (was) as project to use (the now defunct) [jWYSIWYG] jQuery plugin developed by joksnet in Django admin.

[jWYSIWYG]: https://github.com/jwysiwyg/jwysiwyg

A few forks still exist:

* https://github.com/isabella232/jwysiwyg/
* https://github.com/SamkCloud/jwysiwyg/
* https://github.com/TimBroddin/jwysiwyg/
* https://github.com/ouanalyse/jwysiwyg/