import os

from django.conf import settings

JWYSIWYG_MEDIA_ROOT = getattr(
    settings,
    "JWYSIWYG_MEDIA_ROOT",
    os.path.join(os.path.dirname(__file__), "static/jwysiwyg"),
)

JWYSIWYG_MEDIA_URL = getattr(settings, "JWYSIWYG_MEDIA_URL", "/jWYSIWYG/static")
