"""
To use, add into your ``urls.py`` file::

   url(r'^jWYSIWYG/', include('jWYSIWYG.urls'))
"""
# django
from django.urls import re_path

# local
from . import views

app_name = "jWYSIWYG"  # pylint: disable=invalid-name
urlpatterns = [
    re_path(r"^file_manager/$", views.file_manager, name="file_manager"),
    re_path(r"^file_manager.json$", views.file_manager_json, name="file_manager.json"),
]
