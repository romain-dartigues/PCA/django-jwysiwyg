"""jWYSIWYG - basic working example

See: https://github.com/akzhan/jwysiwyg/wiki/File-Manager-API
"""
# stdlib
import json
import os

from django.contrib.auth.decorators import login_required

# django
from django.http import HttpResponse
from django.shortcuts import render
from django.template import RequestContext

# local
from . import settings


def file_manager(request, **kwargs):
    """jWYSIWYG editor with file manager plugin"""
    return render(
        request,
        kwargs.get("template_name", "file_manager.html"),
        RequestContext(request),
    )


# Important: you probably want a more fine-grained permission model;
# for simplicity’s sake, I'm only restricting to connected users.
@login_required
def file_manager_json(request):
    """jWYSIWYG file manager json backend"""
    # FIXME: Full of ugly hacks
    dictionary = {"success": True}

    # 1. first the hard thing, process the `list` requests
    if request.GET.get("action", "auth") == "list":
        dictionary["data"] = {"directories": {}, "files": {}}
        base_dir = request.GET.get("dir", "")
        # FIXME: ugly hack
        # if base_dir, then it begin with JWYSIWYG_MEDIA_URL
        base_dir = base_dir[len(settings.JWYSIWYG_MEDIA_URL) :].lstrip("/")
        directory = os.path.join(
            settings.JWYSIWYG_MEDIA_ROOT,
            base_dir,
        )
        if not os.path.isdir(directory):
            # eek! return an error in the appropriate json format
            return HttpResponse(
                json.dumps(
                    {
                        "success": False,
                        "error": base_dir,
                        "errno": 255,  # XXX: ?
                    }
                ),
                content_type="text/plain; charset=utf-8",
            )
        for entry in os.listdir(directory):
            # if you want to filter by extension, this is the
            # place
            fullpath = os.path.join(directory, entry)
            dictionary["data"]["directories" if os.path.isdir(fullpath) else "files"][entry] = os.path.join(
                settings.JWYSIWYG_MEDIA_URL, base_dir, entry
            )

    # 2. otherwise, processes the default `auth` request
    else:
        # WARNING: I'm not checking the auth code because I think it's
        # not that secure, and I'm lazy, you have been warned.
        dictionary["data"] = {
            "move": {"enabled": False, "handler": "/foo"},
            "rename": {"enabled": False, "handler": "/foo"},
            "remove": {"enabled": False, "handler": "/foo"},
            "mkdir": {"enabled": False, "handler": "/foo"},
            "upload": {"enabled": False, "handler": "/foo/"},
        }

    # 3. returns the JSON answer to the browser
    return HttpResponse(
        json.dumps(dictionary, indent=4),
        content_type="text/plain; charset=utf-8",
    )
